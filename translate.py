import sys, time

from modules.load import load_dataset
from modules.translate import pack_rows, translate_rows, write_combined_rows_to_csv

def main():
    file_reader, dataset = load_dataset(sys.argv[1])

    print("Packing rows...")
    next(dataset)
    packed_rows = pack_rows(dataset)

    print("Translating packed rows...")
    start_time = time.time()
    translated_rows = translate_rows(packed_rows)
    print(f"Translation completed in {time.time() - start_time} seconds")

    print("Writing output...")
    file_reader.seek(0)
    write_combined_rows_to_csv(sys.argv[2], dataset, translated_rows)

if __name__ == "__main__":
    main()