import sys, os, yaml

from pandas import DataFrame

from nltk.sentiment.vader import SentimentIntensityAnalyzer

ARTWORKS_COUNT = 7

def main():
    dataset = yaml.safe_load(open(sys.argv[1]))

    sid = SentimentIntensityAnalyzer()

    polarities = []

    for artwork_id in range(0, ARTWORKS_COUNT):
        opinion = dataset[artwork_id]
        polarity = sid.polarity_scores(opinion)

        polarities.append({
            "artwork_id": artwork_id,
            "opinion": opinion,
            "artist_negative_polarity": polarity["neg"],
            "artist_neutral_polarity": polarity["neu"],
            "artist_positive_polarity": polarity["pos"],
            "artist_compound_polarity": polarity["compound"]
        })

    df = DataFrame(polarities)
    df.to_pickle(f"{sys.argv[2]}/artist.pickle")

if __name__ == "__main__":
    main()