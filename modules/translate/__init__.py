import csv

from argostranslate import translate

OPINION_START_INDEX = 4
OPINION_END_INDEX = 9
ROW_LANGTAG_INDEX = 1
DEST_LANGUAGE = "en"
SUPPORTED_SOURCE_LANGUAGES = ["it"]

def pack_rows(dataset):
    packed_rows = {}

    row_index = 0

    for row in dataset:
        row_lang = row[ROW_LANGTAG_INDEX]
        if row_lang != DEST_LANGUAGE:
            packed_row = row[OPINION_START_INDEX:OPINION_END_INDEX]
            if row_lang not in packed_rows:
                packed_rows[row_lang] = {}

            packed_rows[row_lang][row_index] = packed_row

        row_index += 1

    return packed_rows

def translate_row(translator, row_index, opinions):
    translated_row = []

    current_opinion = 0
    for opinion in opinions:
        current_opinion += 1

        print(f"Translating row {row_index} (opinion {current_opinion}/{len(opinions)}) ({len(opinion)} characters)")
        
        translation_result = translator.translate(opinion)
        translated_row.append(str(translation_result))

    return translated_row

def translate_rows(packed_rows):
    translated_rows = {}

    installed_languages = translate.load_installed_languages()
    language_identifiers = [str(lang) for lang in installed_languages]
    translator = installed_languages[language_identifiers.index("Italian")].get_translation(installed_languages[language_identifiers.index("English")])

    for lang in packed_rows:
        print(f"Translating '{lang}' rows")

        if lang not in SUPPORTED_SOURCE_LANGUAGES:
            print("Unsupported source language")
            continue

        source_rows = packed_rows[lang]

        for (row_index, opinions) in source_rows.items():
            translated_row = translate_row(translator, row_index, opinions)
            translated_rows[row_index] = translated_row

    return translated_rows

def write_combined_rows_to_csv(path, dataset, translated_rows):
    csv_writer = csv.writer(open(path, "w"))

    columns = next(dataset)
    columns.pop(ROW_LANGTAG_INDEX)
    csv_writer.writerow(columns)

    translate_rows_iter = iter(translated_rows.items())
    next_translated_row = list(next(translate_rows_iter))

    row_index = 0

    for row in dataset:
        formatted_row = None

        if next_translated_row and row_index == next_translated_row[0]:
            opinions = next_translated_row[1]
            for i in range(0, len(opinions)):
                opinions[i] = opinions[i].strip()

            formatted_row = row[:OPINION_START_INDEX] + opinions + row[OPINION_END_INDEX:]

            try:
                next_translated_row = list(next(translate_rows_iter))
            except StopIteration:
                next_translated_row = None
        else:
            formatted_row = row

        formatted_row.pop(ROW_LANGTAG_INDEX)
        csv_writer.writerow(formatted_row)

        row_index += 1