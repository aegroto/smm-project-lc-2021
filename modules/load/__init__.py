import csv

def load_dataset(path):
    file_reader = open(path, "r")
    return file_reader, csv.reader(file_reader)
