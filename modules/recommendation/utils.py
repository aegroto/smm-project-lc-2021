def profiles_from_dataframe(D):
    return D.fillna(0) - D.mean(0)