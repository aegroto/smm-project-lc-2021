from langdetect import detect

def __build_aggregated_comments(row):
    return " ".join(row[2:])

def detect_language(row):
    return detect(__build_aggregated_comments(row))