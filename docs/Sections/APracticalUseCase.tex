\section{A practical use case}

Apart from analyical applications, composed of batch processes, the database generated from the pipeline may be suitable for real-time implementations as well. To test this kind of usage, a Telegram bot \cite{telegram_bots} based on the current research results has been realized. It's named \textit{Benny Bot} \cite{benny_bot_live} after the author of the operas used in the project. 

\begin{figure}[!htbp]
    \begin{center}
        \includegraphics[scale=0.5]{Assets/APracticalUseCase/bot_screenshot}
    \end{center}
    \caption{Screenshot of a simple interaction with \textit{Benny Bot}}
\end{figure}

\subsection{The concept}

The way the bot interacts with users is a gamification of the flow of a classic item-item based recommendation system with explicit user feedback gathering. Every time a user asks to play, the bot choose a random artwork from the database, named \textit{pivot artwork}, and records the 3 artworks with most similar features to the \textit{pivot} as well; those lasts pieces are called \textit{similar artworks}. Then, analogously as how the original data collection form did, the bot asks a feedback for each \textit{similar} artwork and one last feedback about the \textit{pivot}. Suddenly, a prediction of the user's sentiment about the pivot is calculated based on their opinion on the \textit{similar} artworks, then compared with the real sentiment expressed by the user and from that comparison the difference between the real and the predicted values is calculated. Every interaction, along with the results, is stored anonimously for future research purposes.

The sentiment analysis capability is again provided by NLTK \cite{nltk} and based on the VADER \cite{vader} algorithm.

The artwork dataset has been extended with 2 previously unused pieces.

\begin{figure}[!htbp]
    \begin{center}
        \includegraphics[scale=0.2]{Assets/Artworks/0.jpeg}
        \includegraphics[scale=0.2]{Assets/Artworks/1.jpeg}
        \includegraphics[scale=0.2]{Assets/Artworks/2.jpeg}
        \includegraphics[scale=0.4]{Assets/Artworks/3.jpeg}
        \includegraphics[scale=0.2]{Assets/Artworks/4.jpeg}
        \includegraphics[scale=0.2]{Assets/Artworks/5.jpeg}
        \includegraphics[scale=0.2]{Assets/Artworks/6.jpeg}
    \end{center}
    \caption{Full artworks dataset used by \textit{Benny bot}}
\end{figure}



\subsection{The prediction algorithm}

The solution used to predict artworks is a slightly modified version of the classic item-item based ratings prediction algorithm. Each object which has the role of predicting ratings is called a \textit{predicter}. Three \textit{predicters} were implemented and, although the code differences are mild, each predicter may lead in very different results from the other in some cases.

\subsubsection{Naive predicter}
The \textit{Naive} predicter is a basic implementation of an item-item ratings prediction solution, very similar to the academic definition of the latter. Each predicted \textit{pivot} polarity value is equal to the normalized weighted mean of the \textit{similar} polarity values, where weights are given by the \textit{distance} of the features of each \textit{pivot-similar} artworks pair.

\begin{lstlisting}[language=Python, breaklines=true]
class NaivePredicter(Predicter):
    def predict_polarities(self, pivot_artwork_id, similar_artwork_ids, similar_polarities):
        distances = manager("prediction_quiz").get_distances(pivot_artwork_id, similar_artwork_ids)

        similar_artwork_profiles = [manager("prediction_quiz").get_artwork_profiles().loc[i] for i in similar_artwork_ids]

        predicted_polarities = dict()
        for polarity_id in POLARITIES_IDENTIFIERS:
            num = 0.0
            den = 0.0

            for i in range(0, len(similar_polarities)):
                polarity = similar_polarities[i][polarity_id]
                distance = distances[i]

                num += polarity * distance
                den += abs(distance)

            predicted_polarities[polarity_id] = num / den

        predicted_polarities = self._transfer_polarities(predicted_polarities)

        return predicted_polarities
\end{lstlisting}

The \textit{polarity transfer} step is an additional heuristic added to handle cases where some polarities have negative values. For example, if the predicted \textit{negative} polarity is a value smaller than 0, it makes sense to increase the \textit{positive} polarity by the same absolute amount and set the \textit{negative} value to zero. In case the \textit{neutral} polarity is smaller than 0, the \textit{positive} and \textit{negative} polarities are incremented by half the absolute of that value and the \textit{neutral} polarity is set to zero. The \textit{compound} polarities are updated accordingly when needed.

\begin{lstlisting}[language=Python, breaklines=true]
        def _transfer_polarities(self, polarities):
        if polarities["neg"] < 0.0:
            variation_factor = -polarities["neg"]

            polarities["pos"] += variation_factor
            polarities["compound"] += variation_factor

            polarities["neg"] = 0.0

        if polarities["pos"] < 0.0:
            variation_factor = -polarities["pos"]

            polarities["neg"] += variation_factor
            polarities["compound"] -= variation_factor

            polarities["pos"] = 0.0

        if polarities["neu"] < 0.0:
            variation_factor = -polarities["neu"]

            polarities["neg"] += variation_factor / 2
            polarities["pos"] += variation_factor / 2

            polarities["neu"] = 0.0

        return polarities
\end{lstlisting}

\subsubsection{Unitary inversion predicter}
This kind of predicter multiplies each weighted \textit{similar} polarity by a value called \textit{inversion factor}. The idea behind the concept of inversion is that \textit{distances} values are calculated over the artist's descriptions, while the polarities are the result of a user's feedback. The user polarities are since compared to the artist polarities over the \textit{similar} artworks and, when the values have opposite sign, the resulting weighted polarity is 'inverted' as well.

\begin{lstlisting}[language=Python, breaklines=true]
class UnitaryInversionPredicter(Predicter):
    def predict_polarities(self, pivot_artwork_id, similar_artwork_ids, similar_polarities):
                ...
                artist_polarity = similar_artwork_profiles[i][POLARITIES_MAP[polarity_id]]
                ...


                inversion_factor = np.sign(artist_polarity * polarity)
                num += inversion_factor * polarity * distance
                ...

        return predicted_polarities
\end{lstlisting}

Parts of code not specified have to be considered identical to the respective lines in the \textit{Naive} predicter class.

\subsubsection{Thresholded inversion predicter}
This predicter is the one used as default by \textit{Benny Bot}, and follows the same principles of the \textit{Unitary inversion} predicter. However, the inversion factor is applied only if the difference between the polarity of the user and the artist's one is higher than a fixed threshold.

\begin{lstlisting}[language=Python, breaklines=true]
class ThresholdedInversionPredicter(Predicter):
    def __init__(self, threshold = 1.0):
        self.__threshold = threshold
    
    def predict_polarities(self, pivot_artwork_id, similar_artwork_ids, similar_polarities):
       ... 

                artist_distance = abs(polarity - artist_polarity)
                inversion_factor = -1.0 if artist_distance > self.__threshold else 1.0 

                uninverted_polarity = polarity * distance

                num += artist_distance * uninverted_polarity * inversion_factor
                ...

        return predicted_polarities
\end{lstlisting}

\subsection{Prediction benchmarking}
\begin{lstlisting}
                                    0       neg       neu       pos  compound
0                      NaivePredicter  0.285680  0.630488  0.255429  0.412973
1           UnitaryInversionPredicter  0.302763  0.519779  0.267565  0.483000
2  0.25-ThresholdedInversionPredicter  0.271551  0.522046  0.184445  0.411998
3   0.5-ThresholdedInversionPredicter  0.270064  0.525226  0.185219  0.414483
4  0.75-ThresholdedInversionPredicter  0.271338  0.544735  0.188202  0.409587
5   1.0-ThresholdedInversionPredicter  0.277245  0.573387  0.206873  0.383253
6  1.25-ThresholdedInversionPredicter  0.274741  0.626857  0.234481  0.388438
7   1.5-ThresholdedInversionPredicter  0.274741  0.626857  0.234481  0.385985
8  1.75-ThresholdedInversionPredicter  0.274741  0.626857  0.234481  0.380914
9   2.0-ThresholdedInversionPredicter  0.274741  0.626857  0.234481  0.380914
\end{lstlisting}

The predicters were tested on the dataset exported by the \textit{translate} task, over 5 various \textit{pivot-similars} combinations. The thresholded inversion predicter seems to be the best performer, returning reasonable error rates when the threshold value is fine tuned.