    \hypertarget{ratings-prediction-based-on-user-profiles}{%
\subsection{Ratings prediction based on user
profiles}\label{ratings-prediction-based-on-user-profiles}}

A dataset containing opinions of users about a set of items holds very
powerful information about people's taste, grouping and influences.
Owning such knowledge makes very easy to run a research to understand
which items are the most liked, which are the less appreciated and,
slipping a bit deeper, what features make the difference on observer's
perceptions when present or missing in an item. However, this kind of
analysis doesn't consider that any moderately large set of people can't
be seen as a whole, aggregated thinking machine, as each individual has
its own taste. As shown in another usage example, even clustering isn't
effective in characterizing the data distribution. A more fine-grained
approach is needed.

A proper analysis and some tuning might be able to build a model that's
capable of estimating a user's rating about an item that person doesn't
know yet, leading the way to build an automated recommendation system.

    \hypertarget{loading-the-dataset}{%
\subsubsection{Loading the dataset}\label{loading-the-dataset}}

The dataset used in the analysis is exported from the pipeline and
contains, for each row, a user's identifier and the compound polarities
relative to each rated artwork.

    \begin{tcolorbox}[breakable, size=fbox, boxrule=1pt, pad at break*=1mm,colback=cellbackground, colframe=cellborder]
\prompt{In}{incolor}{5}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
\PY{k+kn}{import} \PY{n+nn}{pandas}

\PY{n}{users\PYZus{}dataframe} \PY{o}{=} \PY{n}{pandas}\PY{o}{.}\PY{n}{read\PYZus{}pickle}\PY{p}{(}\PY{l+s+sa}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{dataset/sentiment/users.pickle}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
\PY{n}{users\PYZus{}dataframe}\PY{o}{.}\PY{n}{sample}\PY{p}{(}\PY{n}{frac}\PY{o}{=}\PY{l+m+mf}{0.05}\PY{p}{)}
\end{Verbatim}
\end{tcolorbox}

            \begin{tcolorbox}[breakable, size=fbox, boxrule=.5pt, pad at break*=1mm, opacityfill=0]
\prompt{Out}{outcolor}{5}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
     user\_id       0       1       2       3       4
179      179 -0.7269 -0.1027 -0.5719  0.5423 -0.8519
57        57  0.0000  0.0000  0.0000  0.4019  0.0000
88        88  0.0000  0.0000  0.0000  0.0000  0.0000
146      146 -0.6249 -0.5994 -0.2500  0.1531  0.0000
160      160 -0.0772  0.0000 -0.3597  0.0000  0.4215
16        16  0.0000  0.0000 -0.4404  0.0000  0.0000
13        13 -0.5994 -0.1275 -0.3182  0.0000  0.2500
77        77  0.5657  0.4404  0.5994  0.0000  0.7251
181      181  0.0000 -0.5106  0.5994 -0.5106  0.0000
\end{Verbatim}
\end{tcolorbox}
        
    The full dataset is then subdivided into two subsets. Three quarters of
the elements will form the profiles space and used to estimate the
profiles of users we want to predict ratings for, the remaining entries
will be used for evaluation of the prediction procedure.

The function `profiles\_from\_dataframe' is a utility procedure which
fills with zeros missing ratings and normalizes each row by subtracting
the mean row value to each rating. In this case, the actually executed
code will be:

profiles = U.fillna(0) - U.mean(0)

The resulting profiles dataframe will have a nearly identical structure
as the original users' one. The only differences are given by the row
index, now represented by the user\_id column, and the ratings which are
now normalized. Also, as said before, some rows were removed and stored
separately to be used for tests.

    \begin{tcolorbox}[breakable, size=fbox, boxrule=1pt, pad at break*=1mm,colback=cellbackground, colframe=cellborder]
\prompt{In}{incolor}{6}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
\PY{k+kn}{from} \PY{n+nn}{sklearn}\PY{n+nn}{.}\PY{n+nn}{model\PYZus{}selection} \PY{k+kn}{import} \PY{n}{train\PYZus{}test\PYZus{}split}
\PY{k+kn}{from} \PY{n+nn}{modules}\PY{n+nn}{.}\PY{n+nn}{recommendation}\PY{n+nn}{.}\PY{n+nn}{utils} \PY{k+kn}{import} \PY{n}{profiles\PYZus{}from\PYZus{}dataframe}

\PY{n}{train\PYZus{}data}\PY{p}{,} \PY{n}{test\PYZus{}data} \PY{o}{=} \PY{n}{train\PYZus{}test\PYZus{}split}\PY{p}{(}\PY{n}{users\PYZus{}dataframe}\PY{p}{,} \PY{n}{test\PYZus{}size}\PY{o}{=}\PY{l+m+mf}{0.25}\PY{p}{)}

\PY{n}{U} \PY{o}{=} \PY{n}{train\PYZus{}data}\PY{o}{.}\PY{n}{pivot\PYZus{}table}\PY{p}{(}\PY{n}{index}\PY{o}{=}\PY{l+s+s1}{\PYZsq{}}\PY{l+s+s1}{user\PYZus{}id}\PY{l+s+s1}{\PYZsq{}}\PY{p}{)}
\PY{n}{U}\PY{o}{.}\PY{n}{head}\PY{p}{(}\PY{p}{)}

\PY{n}{profiles} \PY{o}{=} \PY{n}{profiles\PYZus{}from\PYZus{}dataframe}\PY{p}{(}\PY{n}{U}\PY{p}{)}
\PY{n}{profiles}\PY{o}{.}\PY{n}{head}\PY{p}{(}\PY{p}{)}
\end{Verbatim}
\end{tcolorbox}

            \begin{tcolorbox}[breakable, size=fbox, boxrule=.5pt, pad at break*=1mm, opacityfill=0]
\prompt{Out}{outcolor}{6}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
                0         1        2         3         4
user\_id
0        0.204728 -0.645389  0.08036  0.205679  0.127243
2       -0.394672  0.247611  0.08036 -0.333421  0.127243
3       -0.091272  0.119611  0.08036 -0.155521  0.127243
4        0.103728 -0.155889  0.08036  0.443879  0.127243
6       -0.113472 -0.390989 -0.54454 -0.155521  0.317343
\end{Verbatim}
\end{tcolorbox}
        
    \hypertarget{the-prediction-algorithm}{%
\subsubsection{The prediction algorithm}\label{the-prediction-algorithm}}

The following code implements a relatively simple approach to ratings
prediction. The rating of the chosen item from the user given as input
is estimated by selecting the nearest N profiles and calculating a
weighted average of their ratings on the item, using the distances from
the user's profile as weights.

The distance between two profiles is represented by the cosine distance
between the features of those profiles.

    \begin{tcolorbox}[breakable, size=fbox, boxrule=1pt, pad at break*=1mm,colback=cellbackground, colframe=cellborder]
\prompt{In}{incolor}{7}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
\PY{n}{cosine} \PY{o}{=} \PY{k}{lambda} \PY{n}{x}\PY{p}{,}\PY{n}{y}\PY{p}{:} \PY{n}{np}\PY{o}{.}\PY{n}{dot}\PY{p}{(}\PY{n}{x}\PY{p}{,} \PY{n}{y}\PY{p}{)} \PY{o}{/} \PY{p}{(}\PY{n}{np}\PY{o}{.}\PY{n}{sqrt}\PY{p}{(}\PY{n}{np}\PY{o}{.}\PY{n}{dot}\PY{p}{(}\PY{n}{x}\PY{p}{,}\PY{n}{x}\PY{p}{)}\PY{p}{)} \PY{o}{*} \PY{n}{np}\PY{o}{.}\PY{n}{sqrt}\PY{p}{(}\PY{n}{np}\PY{o}{.}\PY{n}{dot}\PY{p}{(}\PY{n}{y}\PY{p}{,}\PY{n}{y}\PY{p}{)}\PY{p}{)}\PY{p}{)}

\PY{k}{def} \PY{n+nf}{predict\PYZus{}user\PYZus{}rating}\PY{p}{(}\PY{n}{user\PYZus{}profile}\PY{p}{,} \PY{n}{profiles}\PY{p}{,} \PY{n}{item\PYZus{}id}\PY{p}{,} \PY{n}{neighbourhood\PYZus{}size}\PY{o}{=}\PY{l+m+mi}{3}\PY{p}{)}\PY{p}{:}
    \PY{n}{distances} \PY{o}{=} \PY{n}{profiles}\PY{o}{.}\PY{n}{apply}\PY{p}{(}\PY{k}{lambda} \PY{n}{x}\PY{p}{:} \PY{n}{cosine}\PY{p}{(}\PY{n}{user\PYZus{}profile}\PY{p}{,} \PY{n}{x}\PY{p}{)}\PY{p}{,} \PY{n}{axis}\PY{o}{=}\PY{l+m+mi}{1}\PY{p}{)}

    \PY{n}{selected\PYZus{}distances} \PY{o}{=} \PY{n}{distances}\PY{o}{.}\PY{n}{sort\PYZus{}values}\PY{p}{(}\PY{p}{)}\PY{p}{[}\PY{o}{\PYZhy{}}\PY{n}{neighbourhood\PYZus{}size}\PY{p}{:}\PY{p}{]}

    \PY{n}{similar\PYZus{}users} \PY{o}{=} \PY{n}{selected\PYZus{}distances}\PY{o}{.}\PY{n}{index}
    \PY{n}{similar\PYZus{}users\PYZus{}ratings} \PY{o}{=} \PY{n}{U}\PY{o}{.}\PY{n}{loc}\PY{p}{[}\PY{n}{similar\PYZus{}users}\PY{p}{,} \PY{n}{item\PYZus{}id}\PY{p}{]}
    \PY{n}{predicted\PYZus{}rating} \PY{o}{=} \PY{p}{(}\PY{n}{similar\PYZus{}users\PYZus{}ratings}\PY{o}{*}\PY{n}{selected\PYZus{}distances}\PY{p}{)}\PY{o}{.}\PY{n}{sum}\PY{p}{(}\PY{p}{)}\PY{o}{/}\PY{n}{selected\PYZus{}distances}\PY{o}{.}\PY{n}{sum}\PY{p}{(}\PY{p}{)}
    
    \PY{k}{return} \PY{n}{predicted\PYZus{}rating}
\end{Verbatim}
\end{tcolorbox}

    \hypertarget{performance-measurements}{%
\subsubsection{Performance measurements}\label{performance-measurements}}

To evaluate the prediction efficiency a rating for a fixed item has been
estimated for each user in the test set. Two measures were calculated
from the results:

\begin{itemize}
\tightlist
\item
  MAE (Mean absolute error): represents the average difference between
  the predicted and the real rating.
\item
  Polarity error rate: represents the ratio of ratings which were
  predicted with opposite polarity, given by the sign, in respect to the
  real value.
\end{itemize}

The sample artwork is the fourth in the list and the neighbourhood size
is set to 3.

    \begin{tcolorbox}[breakable, size=fbox, boxrule=1pt, pad at break*=1mm,colback=cellbackground, colframe=cellborder]
\prompt{In}{incolor}{8}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
\PY{k+kn}{import} \PY{n+nn}{numpy} \PY{k}{as} \PY{n+nn}{np}

\PY{n}{ITEM\PYZus{}ID} \PY{o}{=} \PY{l+m+mi}{3}
\PY{n}{N} \PY{o}{=} \PY{l+m+mi}{3}

\PY{n}{TU} \PY{o}{=} \PY{n}{test\PYZus{}data}\PY{o}{.}\PY{n}{pivot\PYZus{}table}\PY{p}{(}\PY{n}{index}\PY{o}{=}\PY{l+s+s1}{\PYZsq{}}\PY{l+s+s1}{user\PYZus{}id}\PY{l+s+s1}{\PYZsq{}}\PY{p}{)}
\PY{n}{test\PYZus{}profiles} \PY{o}{=} \PY{n}{profiles\PYZus{}from\PYZus{}dataframe}\PY{p}{(}\PY{n}{TU}\PY{p}{)}

\PY{n}{predicted\PYZus{}ratings} \PY{o}{=} \PY{p}{[}\PY{p}{]}
\PY{k}{for} \PY{n}{index}\PY{p}{,} \PY{n}{user\PYZus{}profile} \PY{o+ow}{in} \PY{n}{test\PYZus{}profiles}\PY{o}{.}\PY{n}{iterrows}\PY{p}{(}\PY{p}{)}\PY{p}{:}
    \PY{n}{real\PYZus{}rating} \PY{o}{=} \PY{n}{user\PYZus{}profile}\PY{p}{[}\PY{n}{ITEM\PYZus{}ID}\PY{p}{]}
    \PY{n}{user\PYZus{}profile}\PY{p}{[}\PY{n}{ITEM\PYZus{}ID}\PY{p}{]} \PY{o}{=} \PY{l+m+mf}{0.0} \PY{c+c1}{\PYZsh{} Assuming the rating for the chosen item is unknown}

    \PY{n}{predicted\PYZus{}rating} \PY{o}{=} \PY{n}{predict\PYZus{}user\PYZus{}rating}\PY{p}{(}\PY{n}{user\PYZus{}profile}\PY{p}{,} \PY{n}{profiles}\PY{p}{,} \PY{n}{ITEM\PYZus{}ID}\PY{p}{,} \PY{n}{N}\PY{p}{)}

    \PY{n}{error} \PY{o}{=} \PY{n+nb}{abs}\PY{p}{(}\PY{n}{predicted\PYZus{}rating} \PY{o}{\PYZhy{}} \PY{n}{real\PYZus{}rating}\PY{p}{)}

    \PY{n}{predicted\PYZus{}ratings}\PY{o}{.}\PY{n}{append}\PY{p}{(}\PY{p}{\PYZob{}}
        \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{user\PYZus{}id}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:} \PY{n}{index}\PY{p}{,}
        \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{predicted\PYZus{}rating}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:} \PY{n}{predicted\PYZus{}rating}\PY{p}{,}
        \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{real\PYZus{}rating}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:} \PY{n}{real\PYZus{}rating}\PY{p}{,}
        \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{error}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:} \PY{n}{error}\PY{p}{,}
        \PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{polarity\PYZus{}inversion}\PY{l+s+s2}{\PYZdq{}}\PY{p}{:} \PY{n}{np}\PY{o}{.}\PY{n}{sign}\PY{p}{(}\PY{n}{predicted\PYZus{}rating}\PY{p}{)} \PY{o}{!=} \PY{n}{np}\PY{o}{.}\PY{n}{sign}\PY{p}{(}\PY{n}{real\PYZus{}rating}\PY{p}{)}
    \PY{p}{\PYZcb{}}\PY{p}{)}

\PY{n}{results} \PY{o}{=} \PY{n}{pandas}\PY{o}{.}\PY{n}{DataFrame}\PY{p}{(}\PY{n}{predicted\PYZus{}ratings}\PY{p}{)}
\PY{n}{polarity\PYZus{}inversions} \PY{o}{=} \PY{n}{results}\PY{p}{[}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{polarity\PYZus{}inversion}\PY{l+s+s2}{\PYZdq{}}\PY{p}{]}
\PY{n}{mae} \PY{o}{=} \PY{n}{results}\PY{p}{[}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{error}\PY{l+s+s2}{\PYZdq{}}\PY{p}{]}\PY{o}{.}\PY{n}{mean}\PY{p}{(}\PY{p}{)}
\PY{n}{polarity\PYZus{}error\PYZus{}rate} \PY{o}{=} \PY{n}{np}\PY{o}{.}\PY{n}{sum}\PY{p}{(}\PY{n}{polarity\PYZus{}inversions}\PY{p}{)}\PY{o}{/}\PY{n+nb}{len}\PY{p}{(}\PY{n}{results}\PY{p}{)}
\PY{n+nb}{print}\PY{p}{(}\PY{l+s+sa}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{MAE: }\PY{l+s+si}{\PYZob{}}\PY{n}{mae}\PY{l+s+si}{\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
\PY{n+nb}{print}\PY{p}{(}\PY{l+s+sa}{f}\PY{l+s+s2}{\PYZdq{}}\PY{l+s+s2}{Polarity error rate: }\PY{l+s+si}{\PYZob{}}\PY{n}{polarity\PYZus{}error\PYZus{}rate}\PY{l+s+si}{\PYZcb{}}\PY{l+s+s2}{\PYZdq{}}\PY{p}{)}
\PY{n}{results}\PY{o}{.}\PY{n}{sample}\PY{p}{(}\PY{n}{n}\PY{o}{=}\PY{l+m+mi}{10}\PY{p}{)}
\end{Verbatim}
\end{tcolorbox}

    \begin{Verbatim}[commandchars=\\\{\}]
MAE: 0.262335392588707
Polarity error rate: 0.6875
    \end{Verbatim}

            \begin{tcolorbox}[breakable, size=fbox, boxrule=.5pt, pad at break*=1mm, opacityfill=0]
\prompt{Out}{outcolor}{8}{\boxspacing}
\begin{Verbatim}[commandchars=\\\{\}]
    user\_id  predicted\_rating  real\_rating     error  polarity\_inversion
33      134          0.000000     0.257302  0.257302                True
3        13          0.138243    -0.183098  0.321341                True
18       71          0.168293    -0.183098  0.351391                True
43      171          0.144747     0.497702  0.352956               False
4        17          0.000000     0.218802  0.218802                True
26      109          0.060212    -0.183098  0.243310                True
19       72          0.000000    -0.183098  0.183098                True
11       47         -0.116281     0.497702  0.613983                True
44      177          0.000000    -0.183098  0.183098                True
21       87          0.337094     0.359202  0.022108               False
\end{Verbatim}
\end{tcolorbox}
        
    \hypertarget{conclusions}{%
\subsubsection{Conclusions}\label{conclusions}}

Results are not very encouraging. In particular, the error polarity
rate, which is very important in some use cases, is very high. However,
this may be due to the high difference between perceptions of different
humans to the same artwork, which is intrinsic in the research subject.


    % Add a bibliography block to the postdoc
    
    
    
