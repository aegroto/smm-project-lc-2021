from argostranslate import package, translate

# Download from https://drive.google.com/drive/folders/11wxM3Ze7NCgOk_tdtRjwet10DmtvFu3i
package.install_from_path("it_en.argosmodel")
print([str(lang) for lang in translate.load_installed_languages()])

import nltk
nltk.download('vader_lexicon')
