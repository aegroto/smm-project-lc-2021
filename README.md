# Social Media Management project - An analysis about spontaneous human reactions to abstract art

Lorenzo Catania, Università degli Studi di Catania, A.A. 2020-2021.

## Dataset generation
```
./scripts/run_pipeline.sh GFORMS_CSV_PATH ARTWORKS_DESCRIPTIONS_PATH
```

The default artworks descriptions are present in *raw/artwork_descriptions.yaml*

The IT-EN Argos model can be downloaded from https://drive.google.com/drive/folders/11wxM3Ze7NCgOk_tdtRjwet10DmtvFu3i and should be put in the root folder before running the pipeline.

## Docs generation
```
./scripts/build_docs.sh
```

## Run a Benny Bot instance
1. Install PyTG beta from GitHub:
```
pip3 install git+https://github.com/pytg/pytg@pip-package
```

2. Populate the *benny-bot/content/prediction_quiz/dataframes/* folder with the pickled dataframes under *dataset/sentiment/*.

3. Move to the *benny-bot* folder and run *main.py*.
