import sys

from modules.load import load_dataset

def main():
    file_reader, csv_reader = load_dataset(sys.argv[1])
    
    __print_dataset_size(csv_reader)
    reset_reader(file_reader)

    __print_languages_map(csv_reader)

def __print_dataset_size(csv_reader):
    print(f"Dataset size: {get_rows_count(csv_reader) - 1}") 

def __print_languages_map(csv_reader):
    languages_map = build_languages_map(csv_reader)
    print("Entries for each language:")
    for (tag, count) in languages_map.items():
        print(f"\t{tag}: {count}")

def reset_reader(reader):
    reader.seek(0)

def get_rows_count(dataset):
    return sum(1 for _ in dataset)

def build_languages_map(dataset):
    languages_map = {}

    next(dataset)

    for row in dataset:
        language_tag = row[1]
        if language_tag not in languages_map:
            languages_map[language_tag] = 0

        languages_map[language_tag] += 1

    return languages_map

if __name__ == "__main__":
    main()