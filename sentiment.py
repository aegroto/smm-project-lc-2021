import sys, os

from pandas import DataFrame

from nltk.sentiment.vader import SentimentIntensityAnalyzer

from modules.load import load_dataset

ID_COLUMN_NAME = "id"
ARTWORKS_COUNT = 5

def main():
    _, dataset = load_dataset(sys.argv[1])

    columns = next(dataset)

    sid = SentimentIntensityAnalyzer()

    artwork_evaluations = {}
    for artwork_id in range(0, ARTWORKS_COUNT):
        artwork_evaluations[artwork_id] = []

    user_evaluations = []

    for row in dataset:
        entry_id = int(row[columns.index(ID_COLUMN_NAME)])

        user_evaluation = {
            "user_id": entry_id
        }

        for artwork_id in range(0, ARTWORKS_COUNT):
            column_name = f"opinion_{artwork_id}"
            opinion = row[columns.index(column_name)] 
            polarity = sid.polarity_scores(opinion)

            artwork_evaluations[artwork_id].append({
                "user_id": entry_id,
                "opinion": opinion,
                "negative_polarity": polarity["neg"],
                "neutral_polarity": polarity["neu"],
                "positive_polarity": polarity["pos"],
                "compound_polarity": polarity["compound"]
            })

            user_evaluation[artwork_id] = polarity["compound"]

        user_evaluations.append(user_evaluation)

    artworks_db_dir = f"{sys.argv[2]}/artworks"
    os.makedirs(artworks_db_dir, exist_ok=True)

    for (artwork_id, current_artwork_evaluations) in artwork_evaluations.items():
        df = DataFrame(current_artwork_evaluations)
        df.to_pickle(f"{artworks_db_dir}/{artwork_id}.pickle")

    df = DataFrame(user_evaluations)
    df.to_pickle(f"{sys.argv[2]}/users.pickle")

if __name__ == "__main__":
    main()