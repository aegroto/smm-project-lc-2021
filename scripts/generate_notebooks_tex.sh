NB_OUT_FOLDER='docs/Sections/notebooks'
LATEX_OUT_FOLDER_ESCAPED='Sections\/notebooks'
NOTEBOOKS=('aggregate_artwork_sentiment' 'aggregate_users_sentiment' 'user_based_ratings_prediction'  'item_based_ratings_prediction')

for NB in "${NOTEBOOKS[@]}"
do
    jupyter nbconvert --to latex $NB.ipynb --output-dir=$NB_OUT_FOLDER
    OUT_FILES_REPLACE_COMMAND="s/${NB}_files/${LATEX_OUT_FOLDER_ESCAPED}\/${NB}_files/g"
    sed -i.bak -e '1,381d;$d' -e 's/\\subsection/\\subsubsection/g' -e 's/\\section/\\subsection/g' -e $OUT_FILES_REPLACE_COMMAND $NB_OUT_FOLDER/$NB.tex
    rm $NB_OUT_FOLDER/$NB.tex.bak
done