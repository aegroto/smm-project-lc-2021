bold=$(tput bold)
normal=$(tput sgr0)

rm -rf dataset
mkdir dataset/

RAW_GFORMS=$1
ARTWORK_DESCRIPTIONS=$2

echo "${bold}Installing requirements... ${normal}"
python3 install_requirements.py

echo "${bold}Running 'enrich' task on raw database '$RAW_GFORMS'... ${normal}"
python3 enrich.py $RAW_GFORMS dataset/enriched.csv

echo "${bold}Running 'translate' task... ${normal}"
python3 translate.py dataset/enriched.csv dataset/translated.csv

echo "${bold}Running 'sentiment' task... ${normal}"
python3 sentiment.py dataset/translated.csv dataset/sentiment/

echo "${bold}Running 'artist_sentiment' task on descriptions '$ARTWORK_DESCRIPTIONS'... ${normal}"
python3 artist_sentiment.py $ARTWORK_DESCRIPTIONS dataset/sentiment/