from .TextManager import TextManager

def initialize_manager():
    return TextManager()

def depends_on():
    return []