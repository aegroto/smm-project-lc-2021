from .QuizResultsManager import QuizResultsManager

def initialize_manager():
    return QuizResultsManager()

def depends_on():
    return ["data"]