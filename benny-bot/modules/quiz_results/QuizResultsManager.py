import hashlib

from datetime import datetime

from pytg.Manager import Manager
from pytg.load import manager

MODNAME = "quiz_results"

class QuizResultsManager(Manager):
    def save_results(self, chat_id, chosen_artwork, similar_artworks, chosen_polarities, similar_polarities, predicted_polarities):
        timestamp = datetime.now().timestamp()
        result_id = hashlib.sha256(f"{timestamp}{chat_id}".encode('utf-8')).hexdigest()

        for (key, value) in predicted_polarities.items():
            predicted_polarities[key] = float(value)

        manager("data").save_data(MODNAME, "results", result_id, {
            "timestamp": timestamp,
            "chosen_artwork": chosen_artwork,
            "similar_artworks": similar_artworks,
            "chosen_polarities": chosen_polarities,
            "similar_polarities": similar_polarities,
            "predicted_polarities": predicted_polarities
        })
