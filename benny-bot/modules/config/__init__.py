from .ConfigManager import ConfigManager

def initialize_manager():
    return ConfigManager()

def depends_on():
    return []