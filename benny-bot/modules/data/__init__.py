from .DataManager import DataManager

def initialize_manager():
    return DataManager()

def depends_on():
    return []