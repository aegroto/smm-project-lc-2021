import logging, random

from pytg.load import manager

LOGGER = logging.getLogger(__name__)

def start_cmd_handler(update, context):
    bot = context.bot

    message = update.message
    chat_id = message.chat.id

    username = message.from_user.username
    user_id = message.from_user.id

    LOGGER.info("Received start command update from {} ({}) in chat {}".format(username, user_id, chat_id))

    manager("menu").send_menu(bot, chat_id, "prediction_quiz", "start")