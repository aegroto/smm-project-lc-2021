from pytg.load import manager

from telegram.ext import CommandHandler

from .handlers.commands.start import start_cmd_handler

def load_command_handlers(dispatcher):
    dispatcher.add_handler(CommandHandler("start", start_cmd_handler))

def initialize_manager():
    return None

def connect():
    load_command_handlers(manager("bot").updater.dispatcher)

def depends_on():
    return ["bot", "text"]