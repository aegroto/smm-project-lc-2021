import logging, traceback, telegram

from math import dist

from nltk.sentiment.vader import SentimentIntensityAnalyzer

from pytg.load import manager, get_module_content_folder

LOGGER = logging.getLogger(__name__)

def quiz_digester(context, chat_id, form_entries, form_meta):
    LOGGER.info("Quiz digesting ({}, {}, {}, {})".format(chat_id, form_entries, form_meta["similar_artworks"], form_meta["chosen_artwork"]))

    try:
        __send_loading_message(context.bot, chat_id)    

        similar_polarities = __calculate_polarities([
            form_entries["first_artwork_opinion"], 
            form_entries["second_artwork_opinion"],
            form_entries["third_artwork_opinion"]
        ])

        # similar_distances = __get_similar_distances(form_meta["chosen_artwork"], form_meta["similar_artworks"])
        predicter = manager("prediction_quiz").get_default_predicter()
        predicted_polarities = predicter.predict_polarities(
            form_meta["chosen_artwork"], form_meta["similar_artworks"], similar_polarities)

        chosen_polarities = __calculate_polarities([form_entries["chosen_artwork_opinion"]])[0]

        __send_results_message(context.bot, chat_id, chosen_polarities, predicted_polarities)

        manager("quiz_results").save_results(chat_id, form_meta["chosen_artwork"], form_meta["similar_artworks"], chosen_polarities, similar_polarities, predicted_polarities)
    except Exception as e:
        __log_exception(context.bot, chat_id, e)

def __polarity_to_percentage(polarity):
    return polarity * 100.0

def __sentiment_tag_from_compound_polarity(compound_polarity):
    return "well" if compound_polarity >= 0.0 else "bad"

def __send_results_message(bot, chat_id, chosen_polarities, predicted_polarities):
    phrases = __load_phrases()

    real_sentiment = phrases[__sentiment_tag_from_compound_polarity(chosen_polarities["compound"])]
    predicted_sentiment = phrases[__sentiment_tag_from_compound_polarity(predicted_polarities["compound"])]

    text = phrases["results"].format(**{
        "cp_neg": __polarity_to_percentage(chosen_polarities["neg"]),
        "cp_neu": __polarity_to_percentage(chosen_polarities["neu"]),
        "cp_pos": __polarity_to_percentage(chosen_polarities["pos"]),
        "cp_compound": __polarity_to_percentage(abs(chosen_polarities["compound"])),

        "pp_neg": __polarity_to_percentage(predicted_polarities["neg"]),
        "pp_neu": __polarity_to_percentage(predicted_polarities["neu"]),
        "pp_pos": __polarity_to_percentage(predicted_polarities["pos"]),
        "pp_compound": __polarity_to_percentage(abs(predicted_polarities["compound"])),

        "real_sentiment": real_sentiment,
        "predicted_sentiment": predicted_sentiment
    })

    bot.sendMessage(
        chat_id = chat_id,
        text = text,
        parse_mode = telegram.ParseMode.MARKDOWN
    )

def __calculate_polarities(opinions):
    analyzer = SentimentIntensityAnalyzer()

    return [analyzer.polarity_scores(opinion) for opinion in opinions]

def __send_loading_message(bot, chat_id):
    bot.sendMessage(
        chat_id = chat_id,
        text = __load_phrases()["loading"]
    )

def __log_exception(bot, chat_id, e):
    LOGGER.error(f"Unhandled exception: {e}")

    traceback.print_exc()

    bot.sendMessage(chat_id = chat_id, text = __load_phrases()["error"])

def __load_phrases():
    return manager("text").load_phrases("prediction_quiz", "quiz")