from telegram.ext import CallbackQueryHandler

from pytg.load import manager

from .digesters.forms.quiz import quiz_digester

from .handlers.callbacks.quiz import start_quiz_callback_handler

from .PredictionQuizManager import PredictionQuizManager

MODNAME = "prediction_quiz"

def load_digesters():
    manager("forms").add_digester("quiz", quiz_digester)

def load_callback_handlers(dispatcher):
    dispatcher.add_handler(CallbackQueryHandler(start_quiz_callback_handler, "start_quiz"))

def connect():
    manager(MODNAME).load_datasets()

    load_digesters()
    load_callback_handlers(manager("bot").updater.dispatcher)

def initialize_manager():
    return PredictionQuizManager()

def depends_on():
    return ["bot", "config", "text", "forms", "quiz_results"]