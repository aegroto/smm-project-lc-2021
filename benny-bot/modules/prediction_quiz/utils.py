import numpy as np

def cosine_distance(x,y): 
    return np.dot(x, y) / (np.sqrt(np.dot(x,x)) * np.sqrt(np.dot(y,y)))