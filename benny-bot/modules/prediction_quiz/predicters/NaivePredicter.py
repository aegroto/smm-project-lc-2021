import logging

from pytg.load import manager

from .constants import POLARITIES_IDENTIFIERS, POLARITIES_MAP

from .Predicter import Predicter

LOGGER = logging.getLogger(__name__)

class NaivePredicter(Predicter):
    def predict_polarities(self, pivot_artwork_id, similar_artwork_ids, similar_polarities):
        distances = manager("prediction_quiz").get_distances(pivot_artwork_id, similar_artwork_ids)

        similar_artwork_profiles = [manager("prediction_quiz").get_artwork_profiles().loc[i] for i in similar_artwork_ids]

        predicted_polarities = dict()
        for polarity_id in POLARITIES_IDENTIFIERS:
            LOGGER.debug(f"ID: {polarity_id}")

            num = 0.0
            den = 0.0

            for i in range(0, len(similar_polarities)):
                LOGGER.debug(f"Artwork #{i}")

                artist_polarity = similar_artwork_profiles[i][POLARITIES_MAP[polarity_id]]
                polarity = similar_polarities[i][polarity_id]
                distance = distances[i]

                LOGGER.debug(f"Artist polarity: {artist_polarity}")
                LOGGER.debug(f"Polarity: {polarity}")
                LOGGER.debug(f"Distance: {distance}")

                num += polarity * distance
                den += abs(distance)

            LOGGER.debug(f"Num: {num}")
            LOGGER.debug(f"Den: {den}")

            predicted_polarities[polarity_id] = num / den

            LOGGER.debug(f"Result: {predicted_polarities[polarity_id]}")

        predicted_polarities = self._transfer_polarities(predicted_polarities)

        return predicted_polarities