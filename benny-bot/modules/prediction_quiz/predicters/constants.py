POLARITIES_IDENTIFIERS = ['neg', 'neu', 'pos', 'compound']
POLARITIES_MAP = {
    'neg': 'artist_negative_polarity', 
    'neu': 'artist_neutral_polarity', 
    'pos': 'artist_positive_polarity', 
    'compound': 'artist_compound_polarity'
}