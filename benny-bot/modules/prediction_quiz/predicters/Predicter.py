class Predicter:
    def _transfer_polarities(self, polarities):
        if polarities["neg"] < 0.0:
            variation_factor = -polarities["neg"]

            polarities["pos"] += variation_factor
            polarities["compound"] += variation_factor

            polarities["neg"] = 0.0

        if polarities["pos"] < 0.0:
            variation_factor = -polarities["pos"]

            polarities["neg"] += variation_factor
            polarities["compound"] -= variation_factor

            polarities["pos"] = 0.0

        if polarities["neu"] < 0.0:
            variation_factor = -polarities["neu"]

            polarities["neg"] += variation_factor / 2
            polarities["pos"] += variation_factor / 2

            polarities["neu"] = 0.0

        return polarities