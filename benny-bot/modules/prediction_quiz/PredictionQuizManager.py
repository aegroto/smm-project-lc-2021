import pandas
import numpy as np

from pytg.Manager import Manager
from pytg.load import manager, get_module_content_folder

from .utils import cosine_distance

from .predicters.ThresholdedInversionPredicter import ThresholdedInversionPredicter

class PredictionQuizManager(Manager):
    def __init__(self):
        self.__default_predicter = ThresholdedInversionPredicter(0.5)

    def load_datasets(self):
        self.__load_artwork_profiles()
        self.__calculate_artwork_distances()

    def __load_artwork_profiles(self):
        artist_polarities = pandas.read_pickle(f"{get_module_content_folder('prediction_quiz')}/dataframes/artist.pickle").pivot_table(index="artwork_id")
        self.__artwork_profiles = artist_polarities.fillna(0) - artist_polarities.mean(0)

    def __calculate_artwork_distances(self):
        self.__artwork_distances = {}

        for (index, artwork_profile) in self.__artwork_profiles.iterrows():
            distances = self.__artwork_profiles.apply(lambda x: cosine_distance(artwork_profile, x), axis=1)
            distances.sort_values(inplace=True)
            distances.drop(index, inplace=True)
            self.__artwork_distances[index] = distances

    def load_artwork_profiles(self, artwork_indices):
        return [self.__artwork_profiles.iloc[i] for i in artwork_indices]

    def get_artwork_distance_between(self, first_artwork_id, second_artwork_id):
        return self.__artwork_distances[first_artwork_id].loc[second_artwork_id]

    def get_distances(self, pivot_artwork_id, artwork_ids):
        return [self.get_artwork_distance_between(pivot_artwork_id, similar_artwork_id) for similar_artwork_id in artwork_ids]

    def get_artwork_profiles(self):
        return self.__artwork_profiles

    def get_similar_artworks(self, artwork_id, neighbourhood_size=1):
        distances = self.__artwork_distances[artwork_id]

        return distances[-neighbourhood_size:].index.values.tolist()

    def get_default_predicter(self):
        return self.__default_predicter