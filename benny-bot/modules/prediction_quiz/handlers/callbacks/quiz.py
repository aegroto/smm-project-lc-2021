import random, logging

from pytg.load import manager

LOGGER = logging.getLogger(__name__)

def start_quiz_callback_handler(update, context):
    bot = context.bot

    query = update.callback_query
    query_data = query.data.split(",")

    chat_id = query.message.chat_id
    message_id = query.message.message_id

    LOGGER.info("Handling start quiz callback data from {}: {}".format(chat_id, query_data))

    artworks_descriptions = manager("config").load_settings("prediction_quiz", "artworks")

    chosen_artwork = random.choice(list(artworks_descriptions.keys()))

    similar_artworks = manager("prediction_quiz").get_similar_artworks(chosen_artwork, 3)
    random.shuffle(similar_artworks)

    manager("forms").start_form(context, chat_id, "prediction_quiz", "quiz", form_meta = {
        "first_artwork_media": {"type": "photo", "path": artworks_descriptions[similar_artworks[0]]},
        "second_artwork_media": {"type": "photo", "path": artworks_descriptions[similar_artworks[1]]},
        "third_artwork_media": {"type": "photo", "path": artworks_descriptions[similar_artworks[2]]},
        "chosen_artwork": chosen_artwork,
        "similar_artworks": similar_artworks,
        "chosen_artwork_media": {"type": "photo", "path": artworks_descriptions[chosen_artwork]},
    })

    bot.editMessageReplyMarkup(
        chat_id = chat_id,
        message_id = message_id,
        reply_markup = None
    )