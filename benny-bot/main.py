import logging

from pytg.init import boot, initialize, launch

def __main():
    logging.basicConfig(
        format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
        level=logging.INFO
    )

    boot(False)
    initialize()
    launch("bot")

if __name__ == '__main__':
    __main()

