import logging, pandas, csv, sys

from nltk.sentiment.vader import SentimentIntensityAnalyzer

from pytg.init import boot, initialize_module, connect_module

from modules.prediction_quiz.predicters.NaivePredicter import NaivePredicter
from modules.prediction_quiz.predicters.UnitaryInversionPredicter import UnitaryInversionPredicter
from modules.prediction_quiz.predicters.ThresholdedInversionPredicter import ThresholdedInversionPredicter

from dev_modules.prediction_quiz_tests.integration.utils import build_polarities_dict

POLARITY_IDENTIFIERS = ['neg', 'neu', 'pos', 'compound']

ARTWORK_COMBINATIONS = [
    (0, [1, 3, 2]),
    (1, [4, 2, 0]),
    (2, [4, 1, 4]),
    (3, [2, 1, 4]),
    (4, [2, 1, 3]),
]

LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

def run_bench(test_cases, predicter):
    errors = []
    test_case_id = 0
    for (pivot_artwork_id, similar_artwork_ids, similar_polarities, expected_result) in test_cases:
        result = predicter.predict_polarities(pivot_artwork_id, similar_artwork_ids, similar_polarities)

        error = dict()
        for polarity_id in POLARITY_IDENTIFIERS:
            error[polarity_id] = abs(result[polarity_id] - expected_result[polarity_id])

        errors.append(error)

        LOGGER.info(f"Test case #{test_case_id}")
        for polarity_id in POLARITY_IDENTIFIERS:
            LOGGER.info(f"\t{polarity_id} polarity | Predicted: {result[polarity_id]} / Expected: {expected_result[polarity_id]} (Error: {error[polarity_id]})")

        test_case_id += 1

    errors_df = pandas.DataFrame(errors)
    
    return errors_df.mean()

def __build_test_cases(csv_path):
    test_cases = []

    # (pivot_artwork_id, similar_artwork_ids, similar_polarities, expected_result)

    file_reader = open(csv_path, "r")
    dataset = csv.reader(file_reader)

    analyzer = SentimentIntensityAnalyzer()

    for combination in ARTWORK_COMBINATIONS:
        file_reader.seek(0)
        next(dataset)
        for row in dataset:
            pivot_artwork_id = combination[0]
            similar_artwork_ids = combination[1]

            test_cases.append(
                (pivot_artwork_id, similar_artwork_ids, [
                    analyzer.polarity_scores(row[3 + similar_artwork_ids[0]]),
                    analyzer.polarity_scores(row[3 + similar_artwork_ids[1]]),
                    analyzer.polarity_scores(row[3 + similar_artwork_ids[2]]),
                ], analyzer.polarity_scores(row[3 + pivot_artwork_id]))
            )

    return test_cases

if __name__ ==  "__main__":
    boot(True)
    initialize_module("prediction_quiz")
    connect_module("prediction_quiz")

    test_cases = __build_test_cases(sys.argv[1])

    results = []

    results.append(pandas.Series("NaivePredicter").append(run_bench(test_cases, NaivePredicter())))
    results.append(pandas.Series("UnitaryInversionPredicter").append(run_bench(test_cases, UnitaryInversionPredicter())))
    for threshold in [0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0]:
        results.append(pandas.Series(f"{threshold}-ThresholdedInversionPredicter").append(run_bench(test_cases, ThresholdedInversionPredicter(threshold))))

    df = pandas.DataFrame(results)
    print(df)