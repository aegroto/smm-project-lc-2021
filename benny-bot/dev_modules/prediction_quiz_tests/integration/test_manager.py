import pytest

from mockito import mock, when

from pytg.init import boot, initialize_module, connect_module
from pytg.load import manager

from modules.prediction_quiz.PredictionQuizManager import PredictionQuizManager

ERROR_DELTA = 0.1

class TestPredictionQuizManager:
    def setup(self):
        boot(True)

        self.manager: PredictionQuizManager = PredictionQuizManager()

    def test_load_dataset(self):
        self.manager.load_datasets()

    @pytest.mark.parametrize("first_artwork_id,second_artwork_id,expected_distance", [
        (0, 1, -0.63)
    ])
    def test_get_artwork_distances(self, first_artwork_id, second_artwork_id, expected_distance):
        self.manager.load_datasets()

        retrieved_distance = self.manager.get_artwork_distance_between(first_artwork_id, second_artwork_id)

        assert abs(retrieved_distance - expected_distance) < ERROR_DELTA

    @pytest.mark.parametrize("artwork_id,neighbourhood_size,expected_result", [
        (0, 2, [6, 4]),
        (1, 3, [6, 3, 5])
    ])
    def test_get_similar_artworks(self, artwork_id, neighbourhood_size, expected_result):
        self.manager.load_datasets()

        result = self.manager.get_similar_artworks(artwork_id, neighbourhood_size)

        assert result == expected_result