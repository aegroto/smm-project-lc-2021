import pytest

from mockito import mock, when, expect, verifyNoUnwantedInteractions

from pytg.init import boot, initialize_module, connect_module
from pytg.load import manager
from pytg.development import register_mock_manager

from modules.prediction_quiz.digesters.forms.quiz import quiz_digester

REQUIRED_MODULES = ["prediction_quiz"]

def setup():
    boot(True)

    for module in REQUIRED_MODULES:
        initialize_module(module)
        connect_module(module)
    
    register_mock_manager("data", mock())

def __load_phrases():
    return manager("text").load_phrases("prediction_quiz", "quiz")

def test_quiz_digester_no_error():
    context = mock()
    context.bot = mock()

    chat_id = -1

    form_entries = {
        "first_artwork_opinion": "Prison",
        "second_artwork_opinion": "Cancelation",
        "third_artwork_opinion": "Fruit and sun",
        "chosen_artwork_opinion": "Test"
    }

    form_meta = {
        "similar_artworks": [1, 0, 3],
        "chosen_artwork": 2
    }

    expect(context.bot, times=0).sendMessage(chat_id = chat_id, text = __load_phrases()["error"])

    quiz_digester(context, chat_id, form_entries, form_meta)

    verifyNoUnwantedInteractions()