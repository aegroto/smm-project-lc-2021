import sys

from nltk.sentiment.vader import SentimentIntensityAnalyzer

sid = SentimentIntensityAnalyzer()
text = " ".join(sys.argv[1:])
polarities = sid.polarity_scores(text)
print(f"{text} -> ({polarities['neg']}, {polarities['neu']}, {polarities['pos']}, {polarities['compound']})")
