import csv, sys

from modules.load import load_dataset
from modules.enrichment.language_detection import detect_language

COLUMNS = ["id", "lang", "datetime", "device", "opinion_0", "opinion_1", "opinion_2", "opinion_3", "opinion_4"]

def main():
    _, csv_reader = load_dataset(sys.argv[1])
    csv_writer = csv.writer(open(sys.argv[2], "w"))

    next(csv_reader)
    csv_writer.writerow(COLUMNS)

    entry_id = 0
    for row in csv_reader:
        language_tag = detect_language(row)
        enriched_row = [entry_id, language_tag] + row
        csv_writer.writerow(enriched_row)

        entry_id += 1

if __name__ == "__main__":
    main()